<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-boutonstexte?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'boutonstexte_description' => 'Aggiungi pulsanti per la visualizzazione di testi come aumenta/diminuisci il carattere. I pulsanti vengono aggiunti prima di ogni div di classe di testo del contenuto se non sono già presenti nella pagina (textsizeup, textsizedown o immagini di classe solo testo)

La configurazione opzionale può essere eseguita installando il plugin cfg. Alcuni elementi possono essere forzati da javascript.',
	'boutonstexte_nom' => 'Pulsanti in testo',
	'boutonstexte_slogan' => 'Pulsanti per gestire la dimensione dei caratteri'
);
